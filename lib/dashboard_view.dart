import 'dart:convert';
import 'package:catatan_keuangan/login_view.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:catatan_keuangan/kategori_view.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  String saldo = 'Rp. 0';
  String pemasukan = 'Rp. 0';
  String pengeluaran = 'Rp. 0';
  final String apiUrl = 'https://dimaz.site/catatan_keuangan/backend/public/api/saldo';
  final String token = 'your_bearer_token_here'; 

  @override
  void initState() {
    super.initState();
    _getSaldo();
  }

   int _currentIndex = 0;

  // Define your pages that you want to navigate to.
  final List<Widget> _pages = [
    DashboardPage(),
    KategoriPage(),
    LoginPage()
  ];

  
  Future<String?> getBearerToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('bearerToken');
  }
  

  Future<void> _getSaldo() async {
    try {

      final String? bearerToken = await getBearerToken();
      if (bearerToken == null) {
        return;
      }

      print("=====================================");
      print(bearerToken);
      print("=====================================");
      final response = await http.get(
        Uri.parse(apiUrl),
        headers: {
          'Authorization': 'Bearer $bearerToken',
        },
      );

      if (response.statusCode == 200) {
        Map<String, dynamic> responseData = json.decode(response.body);
        setState(() {
          saldo = 'Rp. ${responseData['data']['saldo']}';
          pemasukan = 'Rp. ${responseData['data']['pemasukan']}';
          pengeluaran = 'Rp. ${responseData['data']['pengeluaran']}';
        });
        print('Response: $responseData');
      } else {
        print('Failed to get saldo. Status code: ${response.statusCode}');
      }
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dashboard'),
      ),
      body: Padding(
        padding: EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Card(
              color: Colors.blue,
              child: Padding(
                padding: EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      'Saldo',
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    SizedBox(height: 10),
                    Text(
                      saldo,
                      style: TextStyle(color: Colors.white, fontSize: 24),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 20),
            Card(
              color: Colors.green,
              child: Padding(
                padding: EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      'Pemasukan',
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    SizedBox(height: 10),
                    Text(
                      pemasukan,
                      style: TextStyle(color: Colors.white, fontSize: 24),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 20),
            Card(
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      'Pengeluaran',
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    SizedBox(height: 10),
                    Text(
                      pengeluaran,
                      style: TextStyle(color: Colors.white, fontSize: 24),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });

          // Use Navigator to push the new page onto the stack.
          Navigator.push(context, MaterialPageRoute(builder: (context) => _pages[index]));
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.category),
            label: 'Kategori',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.compare_arrows),
            label: 'Transaksi',
          ),
        ],
      ),
    );
  }
}
