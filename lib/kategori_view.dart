import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:catatan_keuangan/login_view.dart';
import 'package:catatan_keuangan/dashboard_view.dart';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';


class KategoriPage extends StatefulWidget {
  @override
  _KategoriPageState createState() => _KategoriPageState();
}

class _KategoriPageState extends State<KategoriPage> {
  List<dynamic> kategoriList = [];
  final String apiUrl = 'https://dimaz.site/catatan_keuangan/backend/public/api/kategori';
  final String token = 'your_bearer_token_here'; // Ganti dengan token Anda

  @override
  void initState() {
    super.initState();
    _getKategoriList();
  }

  int _currentIndex = 0;

  // Define your pages that you want to navigate to.
  final List<Widget> _pages = [
    DashboardPage(),
    KategoriPage(),
    LoginPage()
  ];

  Future<String?> getBearerToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('bearerToken');
  }

  Future<void> _getKategoriList() async {
    try {

      final String? bearerToken = await 
      getBearerToken();
      if (bearerToken == null) {
        return;
      }

      print("*=====================================");
      print(bearerToken);
      print("*=====================================");

      final response = await http.get(
        Uri.parse(apiUrl),
        headers: {
          'Authorization': 'Bearer $bearerToken',
        },
      );

      if (response.statusCode == 200) {
        List<dynamic> responseData = json.decode(response.body);
        print('Response: $responseData');

        setState(() {
          kategoriList = responseData;
        });
      print("*=====================================");
      print("*=====================================");

      print("*=====================================");
      print("*=====================================");

      } else {
        print('Failed to get kategori list. Status code: ${response.statusCode}');
      }
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Kategori'),
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              // Implementasi logika tambah kategori
              print('Tambah kategori button pressed.');
            },
          ),
        ],
      ),
      body: ListView.builder(
        itemCount: kategoriList.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(kategoriList[index]['nama']),
            subtitle: Text(kategoriList[index]['tipe']),
          );
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });

          // Use Navigator to push the new page onto the stack.
          Navigator.push(context, MaterialPageRoute(builder: (context) => _pages[index]));
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.category),
            label: 'Kategori',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.compare_arrows),
            label: 'Transaksi',
          ),
        ],
      ),
    );
  }
}
